from accelerate import Accelerator
from accelerate.utils import gather_object

import numpy as np
from tqdm import tqdm
import os

from mistral_common.protocol.instruct.tool_calls import Function, Tool
from mistral_inference.model import Transformer
from mistral_inference.generate import generate

from mistral_common.tokens.tokenizers.mistral import MistralTokenizer
from mistral_common.protocol.instruct.messages import UserMessage
from mistral_common.protocol.instruct.request import ChatCompletionRequest

accelerator = Accelerator()
no_ranks = accelerator.num_processes
device = accelerator.device

##### Download model
from huggingface_hub import snapshot_download
from pathlib import Path


access_token = "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" 
mistral_models_path = Path.home().joinpath('mistral_models', '7B-Instruct-v0.3')
if accelerator.is_main_process:
    mistral_models_path.mkdir(parents=True, exist_ok=True)
    snapshot_download(repo_id="mistralai/Mistral-7B-Instruct-v0.3", allow_patterns=["params.json", "consolidated.safetensors", "tokenizer.model.v3"], local_dir=mistral_models_path, token=access_token)

###### Load model 
# mistral_models_path = '/Utilisateurs/pchau/mistral_models/7B-Instruct-v0.3'
tokenizer = MistralTokenizer.from_file(f"{mistral_models_path}/tokenizer.model.v3")
model = Transformer.from_folder(mistral_models_path)

rank = accelerator.process_index
checkpoint_file = f'checkpoint_{rank}.npy'

###### generate question/answer
def get_promt_request(content, category):
    prompt = f"""Generate a concise summary of the following administrative document:
    Category: {category}
    Main paragraph: {content}

    Please follow these instructions:
    1. Only include key information related to the category and main paragraph.
    2. Ensure summary is comprehensive and relevant, making use of as much information from the main paragraph as possible.
    3. At most 3 sentences. 
    """
    
    #     messages = [{"role": "user", "content": prompt}]
    completion_request = ChatCompletionRequest(
    tools=[
        Tool(
            function=Function(
                name="get_summary",
                description="Generate a summary.",
                parameters={
                    "type": "object",
                    "properties": {
                        "summary": {
                            "type": "string",
                            "description": "The summary condenses information in the given paragraph.",
                        },
                    },
                    "required": ["summary"],
                },
            )
        )
    ],
    messages=[
        UserMessage(content=prompt),
        ],
    )
    
    return completion_request

def process_npy_file(file_path):
    # Load the data from the npy file
    data = np.load(file_path, allow_pickle=True)[:20]
    if accelerator.is_main_process: 
        print("Length of Data is :", len(data)) 

    results = []
    if checkpoint_file and os.path.exists(checkpoint_file):
        # Load previous results from checkpoint file
        results = list(np.load(checkpoint_file, allow_pickle=True))
        print(f"Resuming from checkpoint. Loaded {len(results)} results.")    

    # rank = accelerator.process_index
    # print('------------------')
    # print('Rank: ', rank)

    # Process each chunk in parallel using PyTorch's distributed processing
    # sync GPUs and start the timer
    accelerator.wait_for_everyone()
    
    with accelerator.split_between_processes(list(range(len(data)))) as data_id:
        chunk = data[data_id]

        # have each GPU do inference, prompt by prompt
        for i, entry in enumerate(tqdm(chunk)):
            if i < len(results):
                continue  # Skip entries already processed in previous runs
                
            image_path = entry.get('image_path')
            content = entry.get('content')
            category = entry.get('category')

            completion_request = get_promt_request(content, category)
            tokens = tokenizer.encode_chat_completion(completion_request).tokens
#             print('INPUT TOKEN LENGTH:', len(tokens))
            out_tokens, _ = generate([tokens], model, max_tokens=128, temperature=0.0, eos_id=tokenizer.instruct_tokenizer.tokenizer.eos_id)
            print('OUTPUT TOKEN LENGTH:', len(out_tokens[0]))
            result = tokenizer.instruct_tokenizer.tokenizer.decode(out_tokens[0])

            # store outputs and number of tokens in result{}
            results.append({
                "image_path": image_path,
                "content": content,
                "category": category,
                "response": result,
            })
            print(results[-1]['response'])   
            
            # Save results to checkpoint after each iteration
            if checkpoint_file:
                np.save(checkpoint_file, results, allow_pickle=True)
                print(f"Checkpoint saved at iteration {i + 1}.")            

    # collect results from all the GPUs
    results_gathered=gather_object(results)
    
    if accelerator.is_main_process:
        np.save('rlv_cdip_train_p2_sum.npy', results_gathered, allow_pickle=True)
    
    return results_gathered


file_path = '/gpfs/home/pchau/data/rlv_cdip_train_p2.npy'
a = process_npy_file(file_path)