#!/bin/bash

# SLURM OPTIONS
#SBATCH --partition=gpu-a6000  # Partition is a queue for jobs
#SBATCH --time=7-00:00:00     # Time limit for the job
#SBATCH --job-name=JupyterNotebook  # Name of your job
#SBATCH --error=job-%j.err
#SBATCH --output=job-%j.out
#SBATCH --nodes=1       # Number of nodes you want to run your process on
#SBATCH --ntasks-per-node=32     # Number of CPU cores 64/64
#SBATCH --mem=32GB
#SBATCH --gres=gpu:1        # Number of GPUs

PYTHON_VERSION=3.11
ENVIRONMENT_NAME="my_test_env"

module load Anaconda3
# You need this to be able to use the 'conda' command. I am not satisfied with this solution, I'll try to find a way so that you won't have to set it in the future.
source /opt/easybuild/software/Anaconda3/2024.02-1/etc/profile.d/conda.sh


# if the environment does not exist, create it
if ! conda info --envs | grep -q "^${ENVIRONMENT_NAME}"; then
  conda create -n ${ENVIRONMENT_NAME} python=${PYTHON_VERSION} -y
fi

conda activate ${ENVIRONMENT_NAME}

pip install -r requirements.txt


# accelerate launch --multi_gpu --num_processes 4 --mixed_precision='no' pretrain_bart.py --with_tracking 
accelerate launch --num_processes 1 --mixed_precision='no' pretrain_bart.py --with_tracking 
