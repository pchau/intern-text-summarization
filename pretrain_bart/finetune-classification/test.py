import evaluate
import torch
from accelerate import Accelerator
from datasets import load_dataset
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
import numpy as np 
from transformers import (
    BartForSequenceClassification,
    BartTokenizer,
    DataCollatorWithPadding,
)

def main():
    ##### Initialize the accelerator.
    accelerator = Accelerator()
    accelerator.wait_for_everyone()
    
    ##### Load data
    data_files = {}
    data_files['test'] = '/Utilisateurs/pchau/pretrain_bart/finetune-classification/rlv-cdip_test.csv'
    raw_datasets = load_dataset('csv', data_files=data_files) 

    ##### Labels
    is_regression = raw_datasets["test"].features["label"].dtype in ["float32", "float64"]
    if is_regression:
        num_labels = 1
    else:
        label_list = raw_datasets["test"].unique("label")
        label_list.sort()  
        num_labels = len(label_list)
        n = range(num_labels)
        id2label = dict(zip(n,label_list))
        label2id = dict(zip(label_list,n))

    ##### Load model and tokenizer 
    tokenizer = BartTokenizer.from_pretrained('facebook/bart-base')
    if tokenizer.pad_token is None:
        tokenizer.pad_token = tokenizer.eos_token

    model = BartForSequenceClassification.from_pretrained('/Utilisateurs/pchau/pretrain_bart/finetune-classification/output', 
                                                          local_files_only=True, 
                                                          num_labels=num_labels)
    
    ##### Preprocessing the datasets
    def preprocess_function(examples):
        # Tokenize the texts
        text = examples['text']
        result = tokenizer(text, max_length=512, truncation=True) #, padding=padding


        if "label" in examples:
            if label2id is not None:
                # Map labels to IDs
                result["labels"] = [label2id[l] for l in examples["label"]]
            else:
                # In all cases, rename the column to labels because the model will expect that.
                result["labels"] = examples["label"]
        return result
    
    with accelerator.main_process_first():
        processed_datasets = raw_datasets.map(
            preprocess_function,
            batched=True,
            remove_columns=raw_datasets["test"].column_names,
            desc="Running tokenizer on dataset",
        )

    test_dataset = processed_datasets["test"]
    data_collator = DataCollatorWithPadding(tokenizer, pad_to_multiple_of=(8 if accelerator.use_fp16 else None))

    test_dataloader = DataLoader(
        test_dataset, shuffle=True, collate_fn=data_collator, batch_size=64
    )

    ##### Get the metric function
    accuracy = evaluate.load("accuracy")

    ###### Prepare everything with our `accelerator`.
    model, test_dataloader= accelerator.prepare(model,test_dataloader)

    model.eval()
    all_predictions = []
    all_labels = []
    samples_seen = 0
    for step, batch in enumerate(tqdm(test_dataloader, disable=not accelerator.is_local_main_process)):
        with torch.no_grad():
            outputs = model(**batch)

        predictions = outputs.logits.argmax(dim=-1) if not is_regression else outputs.logits.squeeze()
        predictions, references = accelerator.gather((predictions, batch["labels"]))
        # If we are in a multiprocess environment, the last batch has duplicates
        if accelerator.num_processes > 1:
            if step == len(test_dataloader) - 1:
                predictions = predictions[: len(test_dataloader.dataset) - samples_seen]
                references = references[: len(test_dataloader.dataset) - samples_seen]
            else:
                samples_seen += references.shape[0]

        # Append predictions and references to lists
        all_predictions.append(predictions.cpu().numpy())
        all_labels.append(references.cpu().numpy())

        accuracy.add_batch(
            predictions=predictions,
            references=references,
        )

    test_accuracy = accuracy.compute()
    # Combine all predictions and labels
    all_predictions = np.concatenate(all_predictions, axis=0)
    all_labels = np.concatenate(all_labels, axis=0)

    if accelerator.is_main_process: 
        print(f'Accuracy: {test_accuracy}')
        np.save('predictions.npy', all_predictions, allow_pickle=True)
        np.save('labels.npy', all_labels, allow_pickle=True)

if __name__ == "__main__":
    main()