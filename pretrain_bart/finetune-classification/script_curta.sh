#!/bin/bash

# SLURM OPTIONS
#SBATCH --partition=gpu  # Partition is a queue for jobs
#SBATCH --time=4-23:00:00   # Time limit for the job
#SBATCH --job-name=finetune  # Name of your job
#SBATCH --error=job-%j.err
#SBATCH --output=job-%j.out
#SBATCH --nodes=1   # Number of nodes you want to run your process on
#SBATCH --ntasks-per-node=16   # Number of CPU cores
#SBATCH --mem=64GB
#SBATCH --gres=gpu:2    # Number of GPUs

# environment_name="notebook"
# # if the environment does not exist, create it
# if ! conda info --envs | grep -q "^${environment_name}"; then
#   conda create -n ${environment_name} python=3.11 -y
# fi

# conda init
# conda activate $environment_name

# pip install notebook

# python gen_qa.py

# python -m notebook --ServerApp.allow_origin="*" --ServerApp.ip="0.0.0.0"

export NCCL_BLOCKING_WAIT=1
export NCCL_ASYNC_ERROR_HANDLING=1
export NCCL_DEBUG=INFO

nvidia-smi

echo finetune_bart-cdip

accelerate launch --multi_gpu --num_processes 2 finetune_classification.py\
 --checkpointing_steps epoch\
 --output_dir output_bart-cdip
