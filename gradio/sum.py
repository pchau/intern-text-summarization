import gradio as gr 

import pandas as pd
import numpy as np
from PIL import Image

from transformers import pipeline 


###### Load model 
pipe = pipeline("summarization", model="t5-base")

def summarize(text):
    id = int(text)
    image_path = df.image_path[id][0]
    image = Image.open(image_path)
    summary_text = pipe(df.text[id])[0]["summary_text"]
    return image, summary_text #image.resize((400,300))


##### Dataset
def load_cdip_dataset(data_path):
    # Load the dataset
    cdip_single_page_data = np.load(data_path, allow_pickle=True)
    
    # Extract image paths and text entries
    image_entry = [entry.get('Image Path') for entry in cdip_single_page_data]
    text_entry = [entry.get('PgNbr 1') for entry in cdip_single_page_data]
    type_entry = [entry.get('Doc category') for entry in cdip_single_page_data]
    
    # Create a pandas DataFrame
    cdip_data = pd.DataFrame({
        'image_path': image_entry,
        'text': text_entry, 
        'type': type_entry,
    })
    return cdip_data
df = load_cdip_dataset('/Utilisateurs/pchau/data/cdip_signle_page_dataset.npy')

##### GUI
with gr.Blocks() as demo:
    with gr.Row():
        with gr.Column():
            document_image = gr.Image(label="Document Image")
            text = gr.Textbox(label="Document ID")
            summary_btn = gr.Button(value="Summary")
        with gr.Column():
            summary = gr.Textbox(label="Summary text")
    
    summary_btn.click(summarize, inputs=text, outputs=[document_image, summary])
demo.launch(debug=True) # share = True